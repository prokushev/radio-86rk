; mon580-2.asm-Include File for Assembler Program
IncHLAndRetIfEqDEWithBrk equ 0FA8DH
IncHLAndRetIfEqDE equ 0FA90H
GET_HL equ 0FAFFH
PrintCRAndHexWordAndSpaceAndCRAndHexWordAndSpace equ 0FB72H
PrintCRAndHexWordBCAndSpace equ 0FB75H
PrintCharFromA equ 0FCA1H
PrintCRAndHexWordAndSpaceAndHexByteMemAndSpace equ 0FDF4H
PrintHexByteFromMemAndSpace equ 0FDF7H
PrintHexByteAndSpace equ 0FDF8H
BIOSENTRY equ 0FF70H
FARJZ equ 0FFD3H
FARJMP equ 0FFDCH
BDOS equ 0FFF4H
BDOSRET equ 0FFF9H
; Ende Include File for Assembler Program
